<?php
	
    /*
    *------------------------------------------------
    *	Class HomepageDbHelper
    *------------------------------------------------
    */

    // For dev environment 
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', true);
    ini_set('error_reporting', E_ALL);

    // For prod
    //error_reporting(0);
    //ini_set("display_errors", 0);

    // Session
    session_start();

    // Timezone
    date_default_timezone_set('Europe/Paris');

    // Namespace
    use _core\Bootstrap as App;

    // Config location
    define('CONFIG', '_core/config/consts.conf.php');
    define('SITE_CONFIG', '_core/config/site.conf.php');

    // Launch the beast
    try {
        if (defined('SITE_CONFIG') && file_exists(SITE_CONFIG)) {
            require_once(SITE_CONFIG);
        }
        else {
            $error_config = true;
        }

        if (defined('CONFIG') && file_exists(CONFIG)) {
            require_once(CONFIG);	
        }
        else {
            $error_config = true;
        }

        if (defined('BOOTSTRAP') && file_exists(BOOTSTRAP) && !isset($error_config)) {
            require_once(BOOTSTRAP);
            $app = new App($_SERVER['REQUEST_URI']);
        }
        else {
            echo 'Bootstrap not loaded';
        }
    }
    catch (\Exception $e) {
        die($e->getMessage());
    }