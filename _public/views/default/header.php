<!DOCTYPE html>
<html>
<head>
    <title><?= $GLOBALS['data']['sitename'] ?></title>
    <link href="<?= $GLOBALS['data']['css_reset']?>" media="all" type="text/css" rel="stylesheet" />
    <link href="<?= $GLOBALS['data']['css_location']?>bootstrap.min.css" media="all" type="text/css" rel="stylesheet" />
    <link href="<?= $GLOBALS['data']['css_main']?>" media="all" type="text/css" rel="stylesheet" />
</head>

<body class="<?= $GLOBALS['data']['body_class']?>">
<div id="wrapper">