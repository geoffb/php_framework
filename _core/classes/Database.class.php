<?php
	
    /*
    *------------------------------------------------
    *	Class Database
    *------------------------------------------------
    */
    
    namespace _core;

    use \PDO;
    use \PDOException;
	
    if (!defined('CONFIG')) {die('Can not load this file.');}
	
    class Database {
        /**
         * Holds the host
         * @access private
         * @type string
         */
        private $host;

        /**
         * Holds the name
         * @access private
         * @type string
         */
        private $name;

        /**
         * Holds the user
         * @access private
         * @type string
         */
        private $user;

        /**
         * Holds the password
         * @access private
         * @type string
         */
        private $password;

        /**
         * Constructor
         * @param string $host
         * @param string $name
         * @param string $user
         * @param string $password
         * @use $this->host
         * @use $this->name
         * @use $this->user
         * @use $this->password
         *
         */
	function __construct($host = null, $name = null, $user = null, $password = null) {
            if ($host != null && $user != null && $password != null) {
                $this->host = trim($host);
                $this->name = trim($name);
                $this->user = trim($user);
                $this->password = trim($password);
            }
	}

        /**
         * init() - call private function connect()
         * @access public
         * @use $this->connect()
         * @return bool|PDO
         */
        public function init($test = false) {
            return $this->connect($test);
        }

        /**
         * connect() - connect to database with PDO
         * @access private
         * @use $this->host
         * @use $this->name
         * @use $this->user
         * @use $this->password
         * @return bool|PDO
         */
        private function connect($test = false) {
            if (trim($this->host) != null && trim($this->name) != null && trim($this->user) != null && trim($this->password) != null && !$test) {
                try {
                    return new PDO('mysql:host='.$this->host.';dbname='.$this->name, $this->user, $this->password);
                }
                catch(PDOException $e) {
                    return $e;
                }
            }
            elseif ($test) {
	        try {
                    return new PDO('mysql:host='.$this->host, $this->user, $this->password);
                }
                catch(PDOException $e) {
                    return $e;
                }
            }
            else {
                return false;
            }
	}

        /**
         * disconnect() - puts everything to null
         * @access public
         * @use $this->host
         * @use $this->name
         * @use $this->user
         * @use $this->password
         */
        public function disconnect() {
            $this->host = null;
            $this->user = null;
            $this->name = null;
            $this->password = null;
	}
    }