<?php
	
    /*
    *------------------------------------------------
    *	Class Handler
    *------------------------------------------------
    */
    
    namespace _core;
    use \Exception;

    if (!defined('CONFIG')) {die('Can not load this file.');}

    class Handler {
        /**
         * Holds the requested uri
         *
         * @access private
         * @type string
         *
         */
        private $uri;

        /**
         * Holds the controller to be called
         *
         * @access private
         * @type string
         *
         */
        private $controller;

        /**
         * Holds the controller name
         *
         * @access private
         * @type string
         *
         */
        private $controller_name;

        /**
         * Holds the rest of uri - actions
         *
         * @access protected
         * @type string
         *
         */
        private $actions = null;

        /**
         * Constructor
         *
         * @param string $uri the requested uri
         * @use $this->uri
         * @use $this->checkUri()
         * @use $this->init()
         * @throws \Exception
         *
         */
        function __construct($uri = null) {
            if (!is_array($uri)) {throw new Exception('Error: The uri sent to the handler is not right.');}

            $this->uri = $uri;
            $this->checkUri();
            $this->init();
        }

        /**
         * checkUri() - check uri and validity of value
         *
         * @use $this->uri
         * @use Exception
         * @use DEFAULT_LOCATION
         * @set $this->controller
         * @set $this->controller_name
         * @set $this->actions
         * @throws \Exception
         *
         */
        private function checkUri() {
            if (sizeof($this->uri) == 1) {
                if (trim($this->uri[0]) == "") {
                    $this->uri[0] = DEFAULT_LOCATION;
                }
                
                $this->controller = $this->verifyController($this->uri[0]);
                $this->controller_name = $this->uri[0];
            }
            elseif (sizeof($this->uri) > 1) {
                $this->controller = $this->verifyController($this->uri[0]);
                $this->controller_name = $this->uri[0];
                unset($this->uri[0]);
                $this->actions = $this->uri;
            }

            if ($this->controller == null) {
                if (defined('NOT_FOUND')) {
                    $this->controller = $this->verifyController(NOT_FOUND);
                    $this->controller_name = NOT_FOUND;

                    if ($this->controller == null) {
                    	throw new Exception('Error [Handler]: Default location is corrupted - Default file doesn\'t exists.');
                    }
                }
                else {
                    throw new Exception('Error: Nowhere to go!');
                }
            }

            $this->controller_name = ucfirst($this->controller_name);
        }

        /**
         * verifyController() - verification of controller's directory
         *
         * @param null $controller
         * @throws Exception
         * @return string|null
         */
        private function verifyController($controller = null) {
            if (!defined('CONTROLLERS_LOCATION') || !is_dir(CONTROLLERS_LOCATION) || !defined('CONTROLLER_EXT')) {
                throw new Exception('Error: Controllers not set or not a directory.');
            }
            
            if (trim($controller) == null) return null;

            return file_exists(CONTROLLERS_LOCATION.ucfirst($controller).CONTROLLER_EXT) ? CONTROLLERS_LOCATION.ucfirst($controller).CONTROLLER_EXT : null;
        }

        /**
         * verifyView() - verification of view's directory
         *
         * @use VIEWS_LOCATION
         * @use $this->controller_name
         * @throws Exception
         *
         */
        private function verifyView() {
            if (!defined('VIEWS_LOCATION') || !is_dir(VIEWS_LOCATION)) {throw new Exception('Error: no views to display.');}

            if (!is_dir(VIEWS_LOCATION.strtolower($this->controller_name))) {
                throw new Exception('Error: no view to display - folder "'.$this->controller_name.'" missing from views.');
            }
        }

        /**
         * verifyHelper() - verification of view's directory
         *
         * @use VIEWS_LOCATION
         * @use $this->controller_name
         * @throws Exception
         *
         */
        private function verifyHelper() {
            if (!defined('DB_HELPERS') || !is_dir(DB_HELPERS)) {throw new Exception('Error: no helpers\' folder.');}

            if (file_exists(DB_HELPERS.$this->controller_name.DB_HELPERS_EXT)) {
                require_once(DB_HELPERS.$this->controller_name.DB_HELPERS_EXT);
            }
        }

        /**
         * init() - check and start the controller
         *
         * @use $this->uri
         *
         */
        private function init() {
            $is_direct_page = $this->testNeedHtml();

            if (!$is_direct_page) {
                $this->verifyView();
                $this->verifyHelper();
            }
            
            require_once($this->controller);

            new DisplayHandler($this->controller_name, $this->actions, $is_direct_page);
        }
        
        /**
         * Test if the page need html, can be direct page
         * 
         * @return boolean
         */
        private function testNeedHtml() {
            if (!defined('DIRECT_PAGES')) return false;
            
            $unserialized_direct_pages = unserialize(DIRECT_PAGES);
            
            if (!is_array($unserialized_direct_pages) || empty($unserialized_direct_pages)) return false;
            
            return in_array($this->controller_name, $unserialized_direct_pages);
        }
    }