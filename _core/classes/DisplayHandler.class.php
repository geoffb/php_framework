<?php
	
    /*
    *------------------------------------------------
    *	Class DisplayHandler
    *------------------------------------------------
    */
    
    namespace _core;
    use \Exception;

    if (!defined('CONFIG')) {die('Can not load this file.');}

    class DisplayHandler {
        /**
         * Holds the controller to be called
         *
         * @access private
         * @type string
         *
         */
        private $controller;

        /**
         * Holds the rest of uri - actions
         *
         * @access protected
         * @type string
         *
         */
        private $actions = null;

        /**
         * Holds the controller name
         *
         * @access private
         * @type string
         *
         */
        private $controller_name;

        /**
         * Constructor
         *
         * @param null $controller
         * @param null $actions
         * @set $this->controller
         * @set $this->controller_name
         * @set $this->actions
         * @use $this->createController()
         * @throws \Exception
         */
        function __construct($controller = null, $actions = null, $is_direct_page = false) {
            if (trim($controller) == '') {throw new Exception('Error: no controller was sent');}

            $this->controller_name = $controller;

            if (is_array($actions) && !empty($actions)) {
                $this->actions = $actions;
            }

            // Controller
            $this->controller = $this->createController();
            $this->controller->start();
            
            // View
            if (!$is_direct_page)
            	$this->createPage();
        }

        /**
         * createController() - Create the controller
         *
         * @access private
         * @use $this->controller_name
         * @use $this->actions
         * @return object
         */
        private function createController() {
            $controller_class = __NAMESPACE__.'\\'.$this->controller_name;
            
            return new $controller_class($this->actions);
        }

        /**
         * getHeader() - Choose which header to display
         *
         * @access private
         * @use $this->controller_name
         * @throws \Exception
         */
        private function getHeader() {
            if (file_exists(VIEWS_LOCATION.strtolower($this->controller_name).'/'.VIEW_HEADER.VIEW_EXT)) {
                $this->getView(VIEWS_LOCATION.strtolower($this->controller_name).'/'.VIEW_HEADER.VIEW_EXT);
            }
            elseif (file_exists(VIEWS_LOCATION.DEFAULT_VIEW.'/'.VIEW_HEADER.VIEW_EXT)) {
                $this->getView(VIEWS_LOCATION.DEFAULT_VIEW.'/'.VIEW_HEADER.VIEW_EXT);
            }
            else {
                throw new Exception('Error: no header to display.');
            }
        }

        /**
         * getFooter() - Choose which footer to display
         *
         * @access private
         * @use $this->controller_name
         * @throws \Exception
         */
        private function getFooter() {
            if (file_exists(VIEWS_LOCATION.strtolower($this->controller_name).'/'.VIEW_FOOTER.VIEW_EXT)) {
                $this->getView(VIEWS_LOCATION.strtolower($this->controller_name).'/'.VIEW_FOOTER.VIEW_EXT);
            }
            elseif (file_exists(VIEWS_LOCATION.DEFAULT_VIEW.'/'.VIEW_FOOTER.VIEW_EXT)) {
                $this->getView(VIEWS_LOCATION.DEFAULT_VIEW.'/'.VIEW_FOOTER.VIEW_EXT);
            }
            else {
                throw new Exception('Error: no footer to display.');
            }
        }

        /**
         * createPage() - Put everything together
         *
         * @access private
         * @use $this->getHeader()
         * @throws \Exception
         */
        private function createPage() {
            if (!defined('VIEWS_LOCATION') || !is_dir(VIEWS_LOCATION)) {
                throw new Exception('Error: no views to display.');
            }

            if (!defined('DEFAULT_VIEW') || !is_dir(VIEWS_LOCATION.DEFAULT_VIEW)) {
                throw new Exception('Error: Need a default view folder.');
            }

            if (!defined('DEFAULT_VIEW_ACTION') || !defined('VIEW_EXT') || !defined('VIEW_HEADER') || !defined('VIEW_FOOTER')) {
                throw new Exception('Error: Need to config the view\'s constants.');
            }

            $this->getHeader();

            $this->getMainView();

            $this->getFooter();
        }

        /**
         * getView() - Include the view to display
         *
         * @access private
         * @param string $view - file to include
         * @use $this->controller_name
         * @throws \Exception
         */
        private function getView($view = null) {
            if (trim($view) != '' && file_exists($view)) {
                include_once($view);
            }
            else {
                throw new Exception('Error: A view failed to display - file not found.');
            }
        }

        /**
         * getMainView() - include the main view from controller
         *
         * @access private
         * @use $this->controller
         * @throws \Exception
         */
        private function getMainView() {
            if (isset($this->controller->view) && $this->controller->view != null
                && file_exists(VIEWS_LOCATION.strtolower($this->controller_name).'/'.$this->controller->view.VIEW_EXT)) {
                $this->getView(VIEWS_LOCATION.strtolower($this->controller_name).'/'.$this->controller->view.VIEW_EXT);
            }
            elseif (file_exists(VIEWS_LOCATION.DEFAULT_VIEW.'/'.DEFAULT_VIEW_ACTION.VIEW_EXT)) {
                $this->getView(VIEWS_LOCATION.DEFAULT_VIEW.'/'.DEFAULT_VIEW_ACTION.VIEW_EXT);
            }
            else {
                throw new Exception('Error: View from controller does not exists.');
            }
        }
    }