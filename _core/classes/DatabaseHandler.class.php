<?php
	
    /*
    *------------------------------------------------
    *	Class DatabaseHandler
    *------------------------------------------------
    */
    
    namespace _core;

    use _core\Database;

    if (!defined('CONFIG')) {die('Can not load this file.');}

    class DatabaseHandler {
        /**
         * Holds the database connection
         *
         * @access protected
         * @type object
         *
         */
        static protected $db;

        /**
         * init() - Create new database object and set $db
         *
         * @access public
         * @param null $host
         * @param null $name
         * @param null $user
         * @param null $password
         * @return bool|\PDO
         * @use Database
         * @use self::$db
         *
         */
        static public function init($host = null, $name = null, $user = null, $password = null, $test = false) {
            $db = new Database($host, $name, $user, $password);
            $connection = $db->init($test);
            
            if (is_object($connection) && get_class($connection) === 'PDO') {
                self::$db = $connection;
                return true;
            }
            else {
            	self::$db = null;
                return $connection;
            }
        }

        /**
         * close() - disconnect from database
         *
         * @access public
         * @use self::$db
         *
         */
        static public function close() {
            self::$db = null;
        }
    }