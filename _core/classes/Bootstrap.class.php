<?php
	
    /*
    *------------------------------------------------
    *	Class Bootstrap
    *------------------------------------------------
    */
    
    namespace _core;

    use _core\DatabaseHandler;
    use _core\Handler;
	
    if (!defined('CONFIG')) {die('Can not load this file.');}
		
    class Bootstrap {
        /**
        * Holds the requested uri
        * @access private
        * @type string
        */
	private $uri;
		
        /**
        * Constructor
        * @param string $uri the requested uri
        * @use $this->handleUri()
        * @use $this->Start()
        * @use $this->End()
        */
        function __construct($uri = null) {
            $this->handleUri($uri);
            $this->Start();

            $handler = new Handler($this->uri);

            $this->End();
        }
		
        /**
        * handleUri() - replace path in uri and explodes with '/'
        * @access private
        * @param string $uri the requested uri
        * @use $this->uri
        * @use PATH
        */
	private function handleUri($uri = null) {
            $search = (defined('PATH') && PATH != '/') ? PATH : '';
            $uri = str_replace($search, '', $uri);
            $this->uri = explode('/', $uri);

            if (trim($this->uri[0]) == '') {
                unset($this->uri[0]);
                $tmp = array();

                foreach ($this->uri as $key => $elem) {
                    $tmp[$key-1] = $elem;
                }

                $this->uri = $tmp;
            }
	}
		
        /**
        * dumpUri() - var_dump of the uri
        * @access private
        * @use $this->uri
        */
        private function dumpUri() {
            var_dump($this->uri);
        }
		
        /**
        * Start() - start the app
        * @access private
        * @use $this->IncludeFiles()
        * @use  USE_DB
        * @use  DatabaseHandler
        */
        private function Start() {
            $this->IncludeFiles();

            if (defined('USE_DB') && USE_DB) {
                // If you want to use a database, set the credentials first!
                // Database configuration: _core/config/db.conf.php
                //DatabaseHandler::init();
            }
        }

        /**
         * End() - end the app
         * @access private
         * @use  DatabaseHandler
         */
        private function End() {
            //DatabaseHandler::close();
        }
		
        /**
        * IncludeFiles() - include the files needed 
        * @access private
        * @use CLASSES
        * @use CLASSES_LOCATION
        * @use CLASS_EXT
        */
        private function IncludeFiles() {
            if (defined('CLASSES') && is_array(unserialize(CLASSES))) {
                $classes = unserialize(CLASSES);

                if (defined('CLASSES_LOCATION') && defined('CLASS_EXT')) {
                    foreach ($classes as &$class) {
                        $file = CLASSES_LOCATION.$class.CLASS_EXT;

                        if (file_exists($file)) {
                            require_once($file);
                        }
                        else {
                            error_log('The file "'.$file.'" was not found - could not include');
                        }
                    }
                }
            }
        }
    }