<?php
	
    /*
    *------------------------------------------------
    *	Class Controller
    *------------------------------------------------
    */
    
    namespace _core;
    use \Exception;
    use _core\DatabaseHandler;

    if (!defined('CONFIG')) {die('Can not load this file.');}

    class Controller {
        /**
         * Holds the actions
         * @access private
         * @type array
         */
        protected $actions = array();

        /**
         * Holds the view to display
         * @access public
         * @type string
         */
        public $view;

        /**
         * Holds the data to display
         * @access public
         * @type array
         */
        protected $data = array();

        /**
         * Constructor
         * @param array $actions
         * @use $this->actions
         * @throws \Exception
         */
        function __construct($actions = null) {
            if (is_array($actions)) {
                $this->actions = array_values($actions);
                define('CONTROLLER_ACTIONS', serialize($this->actions)); // Static 
            }
            
            $this->view = defined('DEFAULT_VIEW_ACTION') ? DEFAULT_VIEW_ACTION : null;
        }

        /**
         * Display warning
         */
        public function start() {
            // Should be overwritten
            echo '> Warning: You need a start() function in your controller file.';
        }

        /**
         * Puts the data into the GLOBALS var
         * @use $this->data
         * @use $this->verifySitename()
         */
        protected function sendData() {
            $this->verifySitename();
            $this->makeUrl();
            $this->setStylesheets();
            
            $GLOBALS['data'] = $this->data;
        }

        /**
         * Send header to go to location
         * @param null $location - location to go to
         * @return bool
         */
        protected function go($location = null) {
            if (trim($location) == '' || !defined('PATH') || !defined('HTTP')) return false;

            $location = HTTP.'://'.$_SERVER['HTTP_HOST'].PATH.$location;

            header('Location: '.$location);
            exit;
        }

        /**
         * Set an element in data
         * @param null $key
         * @param null $data
         * @use $this->sendData()
         * @use $this->data
         */
        protected function setData($key = null, $data = null) {
            if (trim($key) != null) {
                $this->data[$key] = $data;
            }
        }

        /**
         * Set site name
         * @use $this->data
         * @return bool
         */
        private function verifySitename() {
            if (!defined('SITENAME')) return false;
            
            if (isset($this->data['sitename'])) {
                $this->setData('sitename', $this->data['sitename'].' - '.SITENAME);
            }
            else {
                $this->setData('sitename', SITENAME);
            }
        }
        
        /**
         * Include required controller and it's helper
         */
        static protected function need($controllerCalled = NULL) {
            if (trim($controllerCalled) == '') {
                return false;
            }

            $controller = ucfirst(strtolower($controllerCalled));

            if (file_exists(CONTROLLERS_LOCATION.$controller.CONTROLLER_EXT)) {
                require_once(CONTROLLERS_LOCATION.$controller.CONTROLLER_EXT);

                if (file_exists(DB_HELPERS.$controller.DB_HELPERS_EXT)) {
                    require_once(DB_HELPERS.$controller.DB_HELPERS_EXT);
                }
            }
        }
        
        /**
        * Put the url into data
        * @use $this->setData()
        */
        private function makeUrl() {
            $site_url = HTTP.'://';
            $site_url .= $_SERVER['HTTP_HOST'];
            $site_url .= PATH;

            $this->setData('site_url', $site_url);
        }
        
        /**
        * Set stylesheet
        * @use $this->setData()
        */
        private function setStylesheets() {
            $this->setData('css_location', defined('STYLESHEET_LOCATION') ? $this->data['site_url'].STYLESHEET_LOCATION : null);
            $this->setData('css_reset', defined('STYLESHEET_RESET') ? $this->data['site_url'].STYLESHEET_RESET : null);
            $this->setData('css_main', $this->data['site_url'].$this->getStylesheet());

            // Need js function 
            $this->setData('js_location', defined('JS_LOCATION') ? $this->data['site_url'].JS_LOCATION : null);
        }
        
        /**
        * Get stylesheet
        * @use $this->setData()
        */
        private function getStylesheet() {
            if (defined('STYLESHEET_MAIN') && trim(STYLESHEET_MAIN) != '') {
                return STYLESHEET_MAIN;
            }
        }
        
        /**
        * Start a database connection
        * @use $this->setData()
        */
        static protected function needDb() {
            if (defined('DB_CONF') && file_exists(DB_CONF)) {
            	require_once(DB_CONF);
            }
            
            if (defined('HOST') && defined('USER') && defined('PW') && defined('DB_NAME')) {
	        DatabaseHandler::init(HOST, DB_NAME, USER, PW);
            }
        }
        
        /**
        * Return clean string
        * @param string $string
        * @return string
        */
        static protected function cleanString($string = null) {
            if (trim($string) == '') return null;
        	
            $replace = array('\'', '"', ';', '’', '�');
		
            foreach ($replace as $search) {
		$string = str_replace($search, '', $string);
            }
			
            return htmlspecialchars($string);
        }
        
        /**
        * Test if URL is valid
        * @access static private
        * @param string $url - URL
        * @return bool
        */
        static protected function testUrl($url = null) {
            if (trim($url) == '') return false;

            // GOOD BUT SLOW
            $accepted_responses = array(200, 301);
            $handle = curl_init();

            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_POST, false);
            curl_setopt($handle, CURLOPT_BINARYTRANSFER, false);
            curl_setopt($handle, CURLOPT_HEADER, true);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 1);

            $response = curl_exec($handle);
            $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

            return in_array($httpCode, $accepted_responses) ? true : false;

            // GOOD BUT SLOWER
            /*$headers = get_headers($url, 1); // 1 = sorted as key => array

            print_r($headers[0]);
            echo BR;*/
        }
	
        /**
         * Return the content of html file
         * @param string $file
         * @return string
         */
        static protected function getHtml($file = null) {
            if (!file_exists(VIEWS_LOCATION.$file.'.html')) return 'No content';

            return file_get_contents(VIEWS_LOCATION.$file.'.html');
        }
    }