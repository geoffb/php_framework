<?php
	
    /*
    *------------------------------------------------
    *	Configuration file
    *------------------------------------------------
    */
    
    if (!defined('CONFIG')) {
        die('Can not load this file.');
    }

    define('VERSION_NUMBER', '0.4.3');
    define('HTTP', 'http'); // http or https
    define('PATH', '/'); // Same in .htaccess
    define('_CORE', '_core/');
    define('_PUBLIC', '_public/');
    define('DB_CONF', _CORE . 'config/db.conf.php');
    define('ASSETS', _CORE.'assets/');
    define('CLASSES_LOCATION', _CORE . 'classes/');
    define('CONTROLLERS_LOCATION', _CORE . 'controllers/');
    define('VIEWS_LOCATION', _PUBLIC . 'views/');
    define('DB_HELPERS', CONTROLLERS_LOCATION . 'database_helpers/');
    define('CLASSES', serialize(array('Database',
                                      'DatabaseHandler',
                                      'Handler',
                                      'DisplayHandler',
                                      'Controller')));
    define('STYLESHEET_LOCATION', _PUBLIC . 'css/');
    define('STYLESHEET_RESET', STYLESHEET_LOCATION . 'reset.css');
    define('STYLESHEET_MAIN', STYLESHEET_LOCATION . 'style.css');

    define('JS_LOCATION', _PUBLIC . 'js/');
    define('JS_MAIN', JS_LOCATION . 'script.js');

    define('NOT_FOUND', 'NotFound');
    define('DEFAULT_LOCATION', 'Homepage'); // Default controller
    define('USE_DB', true); // Use database?
    define('SITE_URL', HTTP.'://'.$_SERVER['HTTP_HOST'].PATH);

    $direct_pages = array('Requesthandler');
    define('DIRECT_PAGES', serialize($direct_pages));

    /////////////////////////////////////////////////////////////
    // Other consts
    /////////////////////////////////////////////////////////////
    define('CLASS_EXT', '.class.php');
    define('CONTROLLER_EXT', '.controller.class.php');
    define('DB_HELPERS_EXT', '.helper.class.php');
    define('BOOTSTRAP', CLASSES_LOCATION . 'Bootstrap' . CLASS_EXT);

    define('DEFAULT_VIEW', 'default');
    define('DEFAULT_VIEW_ACTION', 'index');
    define('VIEW_EXT', '.php');
    define('VIEW_HEADER', 'header');
    define('VIEW_FOOTER', 'footer');
    define('SALT', 'ezofibezfdeç!;zfe');