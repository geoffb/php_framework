<?php

    // 404 controller

    namespace _core;

    if (!defined('CONFIG')) {die('Can not load this file.');}
    
    class NotFound extends Controller {
        /**
         * start() - default function
         *
         * @access public
         * @use $this->need()
         * @use Installer::isInstalled()
         * @use $this->sendData()
         * @set $this->view
         *
         */
        public function start() {
            $this->view = 'index';
            $this->setData('sitename', 'Not Found');
            $this->setData('body_class', 'notfound');
            $this->setData('site_url', SITE_URL);
            $this->sendData();
        }
    }