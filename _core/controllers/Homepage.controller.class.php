<?php
	
    /*
    *------------------------------------------------
    *	Class HomepageDbHelper
    *------------------------------------------------
    */

    namespace _core;

    if (!defined('CONFIG')) {die('Can not load this file.');}

    class Homepage extends Controller {

        public function start() {

            // Access the URL from here as array
            //var_dump($this->actions);

            $this->view = 'index';
            $this->setData('sitename', 'Homepage');
            $this->setData('body_class', 'homepage');
            $this->setData('site_url', SITE_URL);
            $this->setData('content', 'Homepage content');
            $this->sendData();
            
            // If you are using database, use like this example:
            //database_helpers\HomepageDbHelper::selectUser();
        }
    }